//
//  APIHandler.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 08/08/23.
//

import Foundation

class APIHandler: NSObject {
    
    private let baseUrl = "https://data.cityofnewyork.us/"
    
    private let resourceUrl = "resource/"
    
    private func getSchoolListUrl(identifier: String) -> String {
        return baseUrl + resourceUrl + identifier + ".json"
    }
    
    private func getSchoolDetailUrl(identifier: String, dbnId: String) -> String {
        return baseUrl + resourceUrl + identifier + ".json?dbn=\(dbnId)"
    }
    
    func getSchoolList(identifier: String, completionHandler: @escaping (([SchoolListItem], String?) -> Void)) {
        let destUrl = getSchoolListUrl(identifier: identifier)
        guard let validUrl = URL(string: destUrl) else {
            completionHandler([], "Invalid Url")
            return
        }
        URLSession.shared.dataTask(with: URLRequest(url: validUrl)) { (data, urlResp, err) in
            guard let validData = data else {
                completionHandler([], "Invalid Data Received")
                return
            }
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            if let schoolList = try? jsonDecoder.decode([SchoolListItem].self, from: validData) {
                print("Decoded Obj is \(schoolList)")
                completionHandler(schoolList, nil)
            } else {
                completionHandler([], "Unable to parse response")
            }
        }.resume()
    }
    
    func getSchoolDetail(dbnId: String, identifier: String, completionHandler: @escaping ((SchoolDetail?, String?) -> Void)) {
        let destUrl = getSchoolDetailUrl(identifier: identifier, dbnId: dbnId)
        guard let validUrl = URL(string: destUrl) else {
            completionHandler(nil, "Invalid Url")
            return
        }
        URLSession.shared.dataTask(with: URLRequest(url: validUrl)) { (data, urlResp, err) in
            guard let validData = data else {
                completionHandler(nil, "Invalid Data Received")
                return
            }
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            if let schoolDetail = try? jsonDecoder.decode([SchoolDetail].self, from: validData), let firstItem = schoolDetail.first {
                print("Decoded Obj is \(schoolDetail)")
                completionHandler(firstItem, nil)
            } else {
                completionHandler(nil, "Unable to parse response")
            }
        }.resume()
    }
}
