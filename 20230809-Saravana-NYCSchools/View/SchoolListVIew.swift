//
//  SchoolListVIew.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 09/08/23.
//

import SwiftUI

struct SchoolListView: View {
    
    @ObservedObject var schoolListViewModel = SchoolListViewModel()
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                ZStack(alignment: .center) {
                    List(schoolListViewModel.schoolDetailsArray, id: \.self) { value in
                        NavigationLink(destination: SchoolDetailView(dpbId: value.dbn)) {
                            RowItemView(schoolItem: value)
                        }
                        .padding([.top, .bottom])
                    }
                    VStack {
                        Text("Loading...")
                        ActivityIndicator(isAnimating: .constant(true), style: .large)
                    }
                    .frame(width: geometry.size.width / 2,
                           height: geometry.size.height / 5)
                    .background(Color.secondary.colorInvert())
                    .foregroundColor(Color.primary)
                    .cornerRadius(20)
                    .opacity(schoolListViewModel.isDataLoading ? 1 : 0)
                }
                .alert("Message", isPresented: .constant(!schoolListViewModel.errorMessage.isEmpty), actions: {
                      Button("Ok", role: .cancel, action: {})
                    }, message: {
                        Text(schoolListViewModel.errorMessage)
                    })
                .navigationTitle("School List")
            }
        }
        .onAppear {
            schoolListViewModel.loadData()
        }
    }
}


struct RowItemView: View {
    
    var schoolItem: SchoolListItem
    
    var body: some View {
        LazyVStack(alignment: .leading) {
            Text(schoolItem.schoolName)
        }
    }
}

struct ActivityIndicator: UIViewRepresentable {
    
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView()
    }
}
