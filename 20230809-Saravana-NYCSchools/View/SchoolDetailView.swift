//
//  SchoolDetailView.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 09/08/23.
//

import SwiftUI

struct SchoolDetailView: View {
    
    @ObservedObject var schoolDetailViewModel = SchoolDetailViewModel()
    
    var dpbId: String = ""
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                VStack(alignment: .leading) {
                    Text("Reading Average Score : \(schoolDetailViewModel.schoolDetail?.satCriticalReadingAvgScore ?? "N/A")")
                    Spacer()
                        .frame(height: 50)
                    Text("Math Average Score : \(schoolDetailViewModel.schoolDetail?.satMathAvgScore ?? "N/A")")
                    Spacer()
                        .frame(height: 50)
                    Text("Writing Average Score : \(schoolDetailViewModel.schoolDetail?.satWritingAvgScore ?? "N/A")")
                }
                .padding(.leading, 15)
                VStack {
                    Text("Loading...")
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }
                .frame(width: geometry.size.width / 2,
                       height: geometry.size.height / 5)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(schoolDetailViewModel.isDataLoading ? 1 : 0)
            }
        }
        .alert("Message", isPresented: .constant(!schoolDetailViewModel.errorMessage.isEmpty), actions: {
              Button("Ok", role: .cancel, action: {})
            }, message: {
                Text(schoolDetailViewModel.errorMessage)
            })
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle(schoolDetailViewModel.schoolDetail?.schoolName ?? "")
        .padding(.top, 40)
        .onAppear {
            schoolDetailViewModel.loadData(dbnId: dpbId)
        }
    }
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(dpbId: "")
    }
}
