//
//  SchoolDetailViewModel.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 09/08/23.
//
import SwiftUI

class SchoolDetailViewModel: ObservableObject {
    
    private let apiHandler = APIHandler()
    
    private let schoolDetailIdentifier = "f9bf-2cp4"
        
    @Published var schoolDetail: SchoolDetail?
    
    @Published var errorMessage = ""
    
    @Published var isDataLoading = false
    
    func loadData(dbnId: String) {
        isDataLoading = true
        apiHandler.getSchoolDetail(dbnId: dbnId, identifier: schoolDetailIdentifier) { [weak self] (schoolDetail, error) in
            guard let weakSelf = self else { return }
            DispatchQueue.main.async {
                weakSelf.isDataLoading = false
                if let validErr = error {
                    weakSelf.errorMessage = validErr
                } else {
                    weakSelf.errorMessage = ""
                    weakSelf.schoolDetail = schoolDetail
                }
            }
        }
    }
}
