//
//  SchoolListViewModel.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 09/08/23.
//

import SwiftUI

class SchoolListViewModel: ObservableObject {
    
    private let apiHandler = APIHandler()
    
    private let schoolListIdentifier = "s3k6-pzi2"
    
    @Published var schoolDetailsArray: [SchoolListItem] = []
    
    @Published var errorMessage = ""
    
    @Published var isDataLoading = false
    
    func loadData() {
        isDataLoading = true
        apiHandler.getSchoolList(identifier: schoolListIdentifier) { [weak self] (schoolItems, error) in
            guard let weakSelf = self else { return }
            DispatchQueue.main.async {
                weakSelf.isDataLoading = false
                if let validErr = error {
                    weakSelf.errorMessage = validErr
                } else {
                    weakSelf.errorMessage = ""
                    weakSelf.schoolDetailsArray = schoolItems
                }
            }
        }
    }
}
