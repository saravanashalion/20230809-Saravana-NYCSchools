//
//  _0230809_Saravana_NYCSchoolsApp.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 08/08/23.
//

import SwiftUI

@main
struct _0230809_Saravana_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
