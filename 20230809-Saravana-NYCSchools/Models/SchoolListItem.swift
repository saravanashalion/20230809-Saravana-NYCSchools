//
//  SchoolListItem.swift
//  20230809-Saravana-NYCSchools
//
//  Created by Saravanakumar Balasubramanian on 09/08/23.
//

import Foundation

// MARK: - SchoolListItem
struct SchoolListItem: Codable, Hashable {    
    let website: String
    let schoolName: String
    let dbn: String
}
